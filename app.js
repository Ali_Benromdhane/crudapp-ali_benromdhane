const express = require("express");
const morgan = require("morgan");

const projectRouter = require("./routes/projectRoutes");
const app = express();

//Middleware

  app.use(morgan("dev"));


app.use(express.json());

//Routes
app.use("/api/projects", projectRouter);

//Display error when user enter invalid route
app.all("*", (req, res, next) => {
  res.status(404).json({
    status:"fail",
    message:`Sorry, there is no route with this ${req.originalUrl}`
  })
});

module.exports = app;
