const express = require("express");
const projectController = require("../controllers/projectController");

const router = express.Router();

// Get All Projects and Create New Project
router
.route("/").get(projectController.getAllProjects)
.post(projectController.createProject)

//Get without tasks projects method
router
  .route("/without-tasks")
  .get(projectController.getProjectsWithoutTasks)

//Get with tasks projects methods
router
  .route("/with-tasks")
  .get(projectController.getProjectsWithTasks)
router
.route("/with-tasks/:id")
.get(projectController.getProjectWithTasks)

//Deletion Methods
router
.route("/delete-done-status")
.delete(projectController.deleteProjectDone)
router
.route("/:id")
.delete(projectController.deleteProject)

//Update Method
router
.route("/:id")
.put(projectController.updateProject)



module.exports = router;
