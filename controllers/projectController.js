const fs = require("fs");
const { nanoid } = require("nanoid");
const projects = JSON.parse(fs.readFileSync(`${__dirname}/../data/project.json`));

exports.getAllProjects = (req,res)=>{

 try{ 
   res.status(200).json({
    status: "success",
    results: projects.length,
    data: {
      projects,
    },
  });
}
  catch(err){
    res.status(404).json({
      status: 'fail',
      message: err
    });
  }

}



exports.getProjectsWithTasks = (req, res) => {

  try{
    const { error } = req.body;
  if (error) {
    res.status(404).send();
  }
  const filteredProjects = projects.filter(project => project.tasks !== undefined)
  res.status(200).json({
    status: "success",
    results: filteredProjects.length,
    data: {
      filteredProjects,
    },
  });
}
catch(err){
  res.status(404).json({
    status: 'fail',
    message: err
  });
}
};



exports.getProjectsWithoutTasks = (req, res) => {
 try{
    const { error } = req.body;
  if (error) {
    res.status(404).send();
  }
  const filteredProjects = projects.filter(project => project.tasks === undefined)
  res.status(200).json({
    status: "success",
    results: filteredProjects.length,
    data: {
      filteredProjects,
    },
  });
}
  catch(err){
    res.status(404).json({
      status: 'fail',
      message: err
    });
  }
};


exports.getProjectWithTasks = (req, res) => {

try{  const ID = req.params.id;
 
  const project  = projects.find(project => project.id === ID)
  res.status(200).json({
    status: "success",
    data: {
      project
    },
  });}
  catch(err){
    res.status(404).json({
      status: 'fail',
      message: err
    });
  }

};


exports.createProject = (req, res) => {

 try{
    const newId = nanoid(3);
  const newProject = Object.assign({ id: newId }, req.body);
  projects.push(newProject);
  
  res.status(201).json({
    status: "success",
    data: {
      project: newProject,
    },
  });
}
  catch(err){
   
      res.status(400).json({
        status: 'fail',
        message: err
      });
  }
  
};


exports.deleteProjectDone = (req, res) => {
 
 try{ const project = projects.find((project) =>  project.status === "done");
  const index = projects.indexOf(project);
  projects.splice(index,1);

  res.status(200).json({
    status: "success",
    data: {
      project
    },
  });}
  catch(err){
    res.status(404).json({
      status: 'fail',
      message: err
    });
  }

};


exports.deleteProject = (req, res) => {
 try{ const ID = req.params.id;
  const project = projects.find((project) =>  project.id === ID);
  const index = projects.indexOf(project);
  projects.splice(index,1);
  
  res.status(200).json({
    status: "success",
    data: {
      project
    },
  });}
  catch(err){
    res.status(404).json({
      status: 'fail',
      message: err
    });
  }

};


exports.updateProject = (req, res) => {

  try{
  const ID = req.params.id;
  const project = projects.findIndex((project) => project.id === ID);

  if (project === -1) {
    res.status(404).send("The project with the given ID not found ");
    return;
  }

  const updatedProject = { ...projects[project], ...req.body };
  projects[project] = updatedProject;

  res.status(200).json({
    status: "success",
    data: {
      project: updatedProject,
    },
  });
}
catch(err){
  res.status(404).json({
    status: 'fail',
    message: err
  });
}
};


